package nl.rubix.service.contractFirst;


import org.apache.camel.builder.RouteBuilder;

public class ContractFirstRouteBuilder extends RouteBuilder  {

	@Override
	public void configure() throws Exception{
		from("cxf:bean:newOrderEndpoint").routeId("contractFirstRoute")
		.recipientList(simple("direct:${header.operationName}"));
		
		from("direct:getOrder")
		.log("operation: getTheOrder")
		.processRef("contractFirstProcessor");
		
		
		from("direct:getTheSpecialOrder")
		.log("operation: GetTheSpecialOrder")
		.processRef("getTheSpecialOrderProcessor");
		
	}
}
