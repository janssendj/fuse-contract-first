package nl.rubix.service.contractFirst;

import java.util.Map;

import nl.rubix.service.xsd.neworder.CustomerType;
import nl.rubix.service.xsd.neworder.GetOrderResponse;
import nl.rubix.service.xsd.neworder.GetTheSpecialOrderRequest;
import nl.rubix.service.xsd.neworder.ObjectFactory;
import nl.rubix.service.xsd.neworder.OrderListType;
import nl.rubix.service.xsd.neworder.OrderType;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class GetTheSpecialOrderProcessor implements Processor {

	@Override
	public void process (Exchange exchange) throws Exception {
		
		Map<String, Object> headers = exchange.getIn().getHeaders();
		for (Map.Entry<String, Object> header : headers.entrySet()){
			System.out.println("headers: " + header.getKey() + "  " + header.getValue() );
		}
		
		
		GetTheSpecialOrderRequest input = exchange.getIn().getBody(GetTheSpecialOrderRequest.class);
		
		
		GetOrderResponse response = createOrderResponse(input);
		
		exchange.getOut().setBody(response);
	}

	private GetOrderResponse createOrderResponse(GetTheSpecialOrderRequest input) {
		ObjectFactory objectFac = new ObjectFactory();
		GetOrderResponse response = objectFac.createGetOrderResponse();
		
		response.setOrderNumber(input.getOrderNumber());
		
		CustomerType customer = getCustomer();
		response.setCustomer(customer);
		
		OrderListType orderList = getOrderList();
		
		response.setOrderList(orderList);
		return response;
	}

	private CustomerType getCustomer() {
		CustomerType customer = new CustomerType();
		customer.setLastname("Rutte");
		customer.setName("Mark");
		return customer;
	}

	private OrderListType getOrderList() {
		OrderType orderType = new OrderType();
		orderType.setPrice(123123);
		orderType.setProductName("Armband");
		
		OrderListType orderList = new OrderListType();
		orderList.getOrder().add(orderType);
		return orderList;
	}
	
}
