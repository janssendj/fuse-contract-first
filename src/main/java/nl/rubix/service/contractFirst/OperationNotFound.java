package nl.rubix.service.contractFirst;

import java.util.Map;

import nl.rubix.service.xsd.neworder.CustomerType;
import nl.rubix.service.xsd.neworder.GetOrderResponse;
import nl.rubix.service.xsd.neworder.GetTheSpecialOrderRequest;
import nl.rubix.service.xsd.neworder.ObjectFactory;
import nl.rubix.service.xsd.neworder.OrderFault;
import nl.rubix.service.xsd.neworder.OrderListType;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class OperationNotFound implements Processor {
	@Override
	public void process (Exchange exchange) throws Exception {
		
		
		OrderFault response = createFaultResponse();
		exchange.getOut().setBody(response);
	}
	
	private OrderFault createFaultResponse() {
		ObjectFactory objectFac = new ObjectFactory();
		OrderFault faultResponse = objectFac.createOrderFault();
		faultResponse.setFaultMessage("Unkown operation");
		return faultResponse;
	}

}
